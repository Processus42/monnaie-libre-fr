// https://sebastianlandwehr.com/blog/creating-an-rss-feed-from-nuxt-content-with-full-body-html-code/

export default async ({ publicRuntimeConfig, config, paths }) => {
  const { $content } = require('@nuxt/content')

  const siteUrl = publicRuntimeConfig.site_url
  const basePath = '/feed'

  // auto fetch all first level dirs in content
  if (!paths) {
    const dirs = await $content('/', { deep: true })
      .where({ dir: { $nin: ['/ui', '/ressources'] } })
      .only('dir')
      .fetch()

    paths = [
      {
        path: '',
        $content: $content({ deep: true }).where({
          dir: { $nin: ['/ui', '/ressources'] },
        }),
      },
      ...[...new Set(dirs.map((item) => item.dir))],
    ]
  }

  const feedTypes = [
    { ext: '.rss', type: 'rss2' },
    { ext: '.xml', type: 'rss2' },
    // { ext: '.atom', type: 'atom1' },
    { ext: '.json', type: 'json1' },
  ]

  const feeds = []

  for (let path of paths) {
    if (typeof path === 'string') path = { path, $content: $content(path) }
    const posts = await path.$content.sortBy('createdAt', 'desc').fetch()

    for (const { ext, type } of feedTypes) {
      const feedOptions = {
        title: config.site_title + path.path,
        link: siteUrl + path.path,
        description:
          path.path === ''
            ? config.site_description
            : `[${path.path}] ${config.site_description}`,
        language: 'fr',
      }

      feeds.push({
        path: basePath + path.path + ext,
        type,
        create: (feed) => {
          feed.options = feedOptions

          posts.forEach((post) => {
            const url = siteUrl + post.path.replace(/^\/pages\//, '/')
            feed.addItem({
              id: url,
              link: url,
              title: post.title,
              description: post.description,
              // author: post.authors,
              date: new Date(post.createdAt),
              content: post.bodyHtml,
            })
          })
        },
      })
    }
  }

  return feeds
}
