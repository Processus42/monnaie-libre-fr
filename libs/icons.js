/*
Icons are here https://fontawesome.com/v5.15/icons?d=gallery&p=2
*/

export default {
  regular: ['faClock'],
  solid: [
    'faHome',
    'faSearch',
    'faSkullCrossbones',
    'faChevronLeft',
    'faChevronRight',
    'faArrowLeft',
    'faArrowRight',
    'faSun',
    'faMoon',
    'faHeart',
    'faInfoCircle',
    'faCheckCircle',
    'faCheck',
    'faExclamationCircle',
    'faExclamationTriangle',
    'faExternalLinkAlt',
    'faBars',
    'faUserCircle',
    'faGlobe',
    'faAngry',
    'faCompressArrowsAlt',
    'faExpandArrowsAlt',
    'faFont',
    'faWheelchair',
    'faCaretDown',
    'faShareAlt',
    'faEnvelope',
    'faComment',
    'faRssSquare',
  ],
  brands: [
    'faCreativeCommonsNcEu',
    'faDiaspora',
    'faDiscourse',
    'faGithub',
    'faGitlab',
    'faYoutube',
    'faRocketchat',
    'faFacebook',
    'faTwitter',
    'faDiscord',
    'faApple',
    'faAndroid',
    'faBitcoin',
    'faChrome',
    'faFirefox',
    'faCodepen',
    'faDev',
    'faDocker',
    'faDropbox',
    'faMastodon',
    'faMedium',
    'faNpm',
    'faReddit',
    'faSlack',
    'faSoundcloud',
    'faSpotify',
    'faSteam',
    'faTeamspeak',
    'faTelegram',
    'faTrello',
    'faVimeo',
    'faWhatsapp',
    'faWikipediaW',
    'faWordpress',
    'faLinkedin',
    'faSkype',
  ],
}
