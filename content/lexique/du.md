---
title: DU
description: 'Dividende Universel et unité de mesure. '
synonyms:
  - dividende universel
  - dividende
---

## Dividende universel

Chaque cocréateur de monnaie libre crée une part de monnaie proportionnelle à la masse monétaire divisée par le nombre de membre.
**DU = c×(M/N)**
Cette création monétaire est appelée Dividende Universel ou DU.
Ce DU est ce qui défini une monnaie comme étant une monnaie libre au sens de la <lexique>TRM<lexique>.

## Unité de mesure.

Dans la monnaie libre Ğ1 ce dividende universel est créé chaque jour sur chaque compte cocréateur de monnaie (un seul compte cocréateur par humain vivant).
La formule est donc dérivée ainsi :
**DU<sub>(t+1)</sub>=DU<sub>(t)</sub> + c<sup>2</sup>×(M/N)<sub>(t)</sub> **. Ce DUğ1 est réévalué tous les 6 mois (aux équinoxes).
Ce DUğ1 est un invariant spatio-temporel : quelque-soient le lieu et l'époque un être humain crée 1 DUğ1 par jour, et ce DUğ1 représente toujours la même portion de la masse monétaire globale (approximativement).

En utilisant ce DUğ1 quotidien comme unité de mesure, l'evolution des prix ne dépends pas de la masse monétaire, mais de facteurs extérieurs à la monnaie : offre/demande, météo et autres impondérables.
