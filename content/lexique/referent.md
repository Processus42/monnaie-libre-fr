---
title: Référent
description: Les cocréateurs les plus actifs sur la toile de confiance
---

Les cocréateurs référents sont les humains *les plus actifs sur la <lexique title="TdC">toile de confiance</lexique>*, c'est-à-dire ceux qui ont reçu **et** émis un certain nombre de certifications.

Un membre de la <lexique>TdC</lexique> Ğ1 est membre référent lorsqu'il a reçu et émis au moins Y[N] certifications où N est le nombre de membres de la TdC et Y[N] = plafond (N<sup>(1/5)</sup>).

Autrement dit l'arrondi à l'entier supérieur de la racine cinquième du nombre de membres.

Exemples :

- Pour 1 024 < N ≤ 3 125 on a Y[N] = 5
- Pour 7 776 < N ≤ 16 807 on a Y[N] = 7
- pour 59 049 < N ≤ 100 000 on a Y[N] = 10

Car

- 5<sup>5</sup> = 3 125
- 6<sup>5</sup> = 7 776
- 7<sup>5</sup> = 16 807
- 9<sup>5</sup> = 59 049
- etc

*Actuellement, un membre est référent s'il a reçu au moins 7 certifications **et** émis au moins 7 certifications.* 

Les référents **ne servent que de repère** pour calculer la  <lexique>règle de distance</lexique>. C'est 80% de ces référents dont il faut être à moins de 5 pas.