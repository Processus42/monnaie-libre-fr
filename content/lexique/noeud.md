---
title: Nœud
description: Un nœud est un ordinateur qui fait fonctionner la monnaie.
synonyms:
  - noeud
  - nœuds
  - noeuds
---
Les nœuds sont les ordinateurs équipés du logiciel Duniter qui gère la <lexique>blockchain</lexique> de la Ğ1.
Ces ordinateurs sont mis à disposition par des utilisateurs de la Ğ1.
Ce sont de petites machines, le plus souvent des ordinateurs individuels micro-ordinateurs voire nano-ordinateurs.

Chaque nœud Duniter vérifie que le contenu de chaque bloc respecte toutes les règles de la Ğ1. Le calcul d’un bloc est conçu pour durer en moyenne 5 minutes

Seuls les nœuds portant la clé privée d'un cocréateur (membre de la toile de confiance) peuvent écrire ce bloc dans la blockchain. Ce sont les "forgeurs"

Les nœuds portant une clé privée qui n'est pas celle d'un cocréateur ne peuvent pas écrire dans la blockchain, ils sont utiles pour communiquer avec les clients. Ce sont les nœuds "miroirs".

Actuellement le <lexique>client</lexique> césium oblige à paramétrer un nœud Duniter. Le mieux est de ne pas choisir le même nœud que vos voisins pour répartir la charge de travail.

