---
title: Licence
description: Un engagement des cocréateurs
---
La licence est un résumé des règles à respecter pour devenir membre de la toile de confiance.

Toute personne désirant participer à la cocréation monétaire doit lire, comprendre et accepter la [Licence](/licence-g1).

C'est un engagement auprès de tous les utilisateurs.