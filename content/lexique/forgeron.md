---
title: Forgeron
description: Membre qui fait tourner Duniter
synonyms:
  - Forgeur
---
La Ğ1 fonctionne grâce à une blockchain décentralisée.  
Une blockchain décentralisée fonctionne avec des ordinateurs mis à disposition pour la communauté.    
Les forgerons sont les utilisateurs qui mettent à disposition une partie de leur ordinateur en installant le logiciel Duniter.

Ces ordinateurs (instances Duniter) sont appelés <lexique>nœuds</lexique>. 