---
title: Monnaie pleine
description: "Quand le nombre de cocréateur de monnaie sera stable. "
---
Dans le monde de la monnaie libre, ce que nous appelons monnaie pleine est le moment ou la moyenne en nombre de DU par membre sera stable. 

    
Quand le nombre de cocréateur ne bouge presque plus ( N stable ) la  réévaluation du DUğ1 est très très proche de 4,88% par semestre.   
Donc tous les six mois le nombre de DUğ1 existants est divisé par 1,0488.

Après environs 40 ans d'existence, un compte cocréateur arrive à environ 3925 DUğ1 en fin de semestre. L'application  de la réévaluation ( division par  1,0488) ramène le compte aux environs de 3742 DUğ1.    
En un semestre un cocréateur crée 182 ou 183 DUğ1.   
3742  + 183  = 3925    
Tout au long du semestre la quantité de DUğ1 créés oscille entre 3742 et 3925 par cocréateur de monnaie. Il est impossible de créer plus de DUğ1.  

Donc la monnaie pleine c'est quand le nombre de membre est stable depuis au moins 40 ans.