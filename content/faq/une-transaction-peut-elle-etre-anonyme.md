---
title: Une transaction peut-elle être anonyme ?
description: Un logiciel est en cours de développement.
---
## Pas encore
Le protocole actuel ne permet pas de rendre directement une transaction anonyme. (la liste des transactions envoyées ou reçues par un compte est publique, et l'émetteur et le récepteur de chaque transaction sont publics)

Un tel fonctionnement est nécessaire pour la sécurité de la monnaie, d'un point de vue technique. Cependant, il est possible de rendre une transaction anonyme par la technique du “mixage” (couramment utilisée avec le Bitcoin) : plusieurs personnes envoient une transaction à un compte, qui renvoie ces transactions à d'autres. On ne peut plus savoir qui a envoyé à qui.

Un logiciel permettant de “mixer” facilement et de manière sécurisée (le premier mixeur monétaire sécurisé du monde !) est en cours de développement ([plus d'infos](https://zettascript.org/projects/gmixer/)).

## En attendant.

P﻿our préserver votre anonymat, vous pouvez ouvrir plusieurs comptes, qui ne seront pas créateurs de monnaie, et utiliser ces comptes de manière anonyme, en n'y indiquant ni identité ni commentaire.
En vous arrangeant pour recevoir et faire des paiements sur des comptes eux mêmes anonymes. 
