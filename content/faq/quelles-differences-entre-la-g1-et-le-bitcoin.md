---
title: "Quelles différences entre la Ğ1 et le Bitcoin ?"
description: La création d'un bitcoin est basée sur une puissance de calcul. La Ğ1 n’est pas spéculative.
---
## La création monétaire

Les bitcoins sont créés en récompensant les propriétaires des ordinateurs qui font fonctionner un nœud de la blockchain. La création monétaire est basé sur une puissance de calcul, d'où sa forte consommation énergétique.
Avec le temps de moins en moins de bitcoins sont créés. Vers 2140 il n'y aura plus de création de bitcoins, il faudra faire avec ceux existant. De plus, les premiers "mineurs" ont pu créer des bitcoins beaucoup plus facilement que les mineurs d'aujourd'hui ; il n'y a donc pas d'égalité de création monétaire dans le temps.

A l'inverse les Ğ1 sont créées à part égale entre tous les cocréateurs (qu'ils fassent tourner la blockchain ou pas). L'humain est au cœur de la création monétaire.
Avec le temps de plus en plus de Ğ1 sont créées (proportionnellement à la masse monétaire).

Là où le bitcoin favorise ceux qui sont en mesure de faire tourner un <lexique>nœud</lexique> (financièrement et techniquement) la Ğ1 ne favorise personne.

## La Ğ1 n’est pas un actif spéculatif

La rareté programmée du bitcoin, en fait un candidat à la spéculation. Plus c'est rare plus c'est cher (par rapport aux autres monnaies en moyenne).

A contrario la monnaie libre étant de plus en plus abondante en unités, ne peut pas vraiment être une réserve de valeur sur le long terme.

De plus, comme le mètre, le gramme ou la seconde, la monnaie libre est un véritable instrument de mesure des valeurs économiques fiable dans le temps, en utilisant le <lexique>DU</lexique> comme unité comparative.

## La Ğ1 a-t-elle des points communs avec le Bitcoin ?

Comme le bitcoin la Ğ1 utilisé une blockchain.
Mais l’algorithme a été amélioré pour être un très [faible consommateur d'énergie](/faq/la-g1-est-elle-un-goufre-energetique)

Mais **la monnaie libre n'est pas que la Ğ1**.
Le concept de monnaie libre est indépendant de la technologie employé.