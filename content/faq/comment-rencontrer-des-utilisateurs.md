---
title: Comment rencontrer des utilisateurs
description: Voir les groupe  locaux ou la carte des membres
---
Plusieurs solutions s'offrent à vous : 


## Solution n°1 : Participer aux rencontres. 
La plupart des rencontres sont indiquées sur la carte et [le calendrier du forum](https://forum.monnaie-libre.fr/calendar).  

Pour ne pas rater une rencontre, vous pouvez suivre la catégorie correspondant à votre région et ainsi recevoir une notification à chaque nouvel évènement.    
Ce petit tuto fait par un Breton [comment ne pas rater une rencontre](https://forum.monnaie-libre.fr/t/comment-ne-pas-rater-une-rencontre/7408)  peut être adapté à chaque catégorie locale.    

Vous pouvez aussi vous inscrire dans un groupe du forum correspondant à votre département : [utilisation des groupes de notification par départements](https://forum.monnaie-libre.fr/t/utilisation-des-groupes-de-notification-par-departements/20869/1)

Voir aussi : [comment faire quand on est seul](/faq/comment-faire-quand-on-est-seul)



## Solution n°2 : Contacter votre groupe local

Il arrive parfois que les membres s'organisent sous forme de "groupe local".

Un "groupe local" est simplement un groupe d'être humains qui choisissent, de leur propre chef, de se doter d'un ou de plusieurs outil(s) pour communiquer entre eux et organiser des rencontres.

Le moyen de communication choisi varie d'un groupe à l'autre ; il peut s'agir : 

* d'un groupe de **discussion** par e-mail ("*mailing list*") 
* d'un groupe *Facebook*, d'un canal *Discord*, d'un *chat*, d'un *forum* ou d'un *site web*
* Ou simplement du forum général de la monnaie libre, il y a une [catégorie par région](https://forum.monnaie-libre.fr/) 

 [Voir la carte des évènements et groupes locaux](https://carte.monnaie-libre.fr?members=false) 
reprenant les informations du forum. 

Les différents canaux de communications des groupes locaux sont fréquemment indiqués sur le [forum](https://forum.monnaie-libre.fr/) dans le sujet épinglé en haut de la catégorie groupe local correspondant à votre région. Si ce n'est pas le cas, n'hésitez pas à les ajouter vous même quand vous en découvrez de nouveaux.   


## Solution n°3 : Contacter directement les membres qui habitent près de chez vous

L'application cliente Cesium dispose d'une surcouche qui la dote d'un annuaire, qui vous permet de contacter directement les membres qui habitent près de chez vous.

Dans Cesium, rendez-vous dans l'onglet Annuaire puis dans Carte.  
Pour plus de confort d'utilisation, faites-le plutôt sur ordinateur que sur mobile

Vous pouvez aussi consulter la carte en ligne (mais vous ne pourrez pas vous connecter à votre compte pour envoyer un message) : 
[Voir la carte des utilisateurs de la Ğ1 dans césium démo](https://demo.cesium.app/#/app/wot/map?c=46.6042:3.6475:6).   (cette carte est souvent longue à télécharger)  
Préférez la [world wotmap](https://worldwotmap.coinduf.eu/)

La carte qui regroupe maintenant toutes ces informations est celle en page d'accueil de ce site  [Voir la carte complète en plein écran](https://carte.monnaie-libre.fr/) 