---
title: Quelles différences avec les SEL (Systemes d'Echanges Locaux)
description: Du lien et des échanges sans limites
---
Les SEL utilise comme monnaie d'échange des unités qui correspondent à du temps. Le plus souvent une minute pour une unité.  
Ces unités monétaire (minutes) sont crées et détruites aux cours des échanges par débits et crédits mutuels, sans contrôle.   

*Il est impossible de savoir combien il y a d'unité en circulation.    
La triche est assez facile, cela oblige à avoir une grande confiance.*  

Cette absence de contrôle de la monnaie dans un SEL est le plus souvent compensée par des limites : 
* débit et crédits maximum sur un compte, 
* montant maxi des échanges, 
* pas d'échange de type professionnel, 
* localisation des échanges, 
* impossibilité de transmettre son compte par héritage,
* ... 

La monnaie libre permet s’affranchir de toutes ces limites. Car la monnaie est crée de façon contrôlée par la formule mathématique **DU<sub>(t+1)</sub>=DU<sub>(t)</sub> + c<sup>2</sup> × (M/N)<sub>(t)</sub>**

Un SEL peut choisir d'utiliser la monnaie libre pour ses échanges, ce qui lui permet de s'affranchir de ces limites.