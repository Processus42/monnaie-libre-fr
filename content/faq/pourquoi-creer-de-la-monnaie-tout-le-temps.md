---
title: Pourquoi créer de la monnaie tout le temps
description: Pour une creation monétaire soutenable
---

## Contre les inégalités. 
Si on décide de partir tous avec la même quantité de monnaie, avec le temps certains vont capter plus de monnaie que d'autres. Et plus on a de monnaie au départ plus on peut capter de la monnaie.   
Au fil du temps les inégalité vont se creuser, jusqu’à bloquer le système car quelques personnes auront capté toute la monnaie. 

## Pour le renouvellement des générations.   
Le phénomène s’aggrave avec le renouvellement des génération. Ceux dont les parents ont gagné beaucoup de monnaie commence dans la vie avec un énorme avantage sur ceux dont les parents n'ont pas pu gagner autant.    
La monnaie des arrières grands parents influe sur leurs arrières petits enfants alors qu'ils ne se sont jamais rencontrés.

## Pour éviter les à-coups.
Si on décidait de créer une quantité fixe par personne, il faudrait savoir à quel age créer cette monnaie.   
Il est impossible de déterminé un age, chaque individu s'émancipe à un age différent.    
Si un individu disparaît prématurément sa part de monnaie serait trop importante par rapport à l'usage qu'il en a eu. L'influence de cette part de monnaie serait trop importante.    
La création monétaire serait chaotique. 

## Création en continue
Une création continue permet de ne jamais manquer de monnaie, de la repartir à la création et d'avoir une évolution lisse et prévisible de la masse monétaire. 