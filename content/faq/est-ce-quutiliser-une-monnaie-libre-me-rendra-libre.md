---
title: Est-ce qu'utiliser une monnaie libre me rendra libre ?
description: Non, mais ça y participe.
---
La monnaie libre est compatible avec les quatre libertés économiques fondamentales définies dans la <lexique>TRM</lexique>, mais ne garantit pas leur respect. Si une des quatre libertés est empêchée, même par un autre facteur que la monnaie, alors cette dernière n'est plus libre :

  1.  **Liberté de choisir** : Si une monnaie est imposée ou interdite, alors cette liberté est supprimée. Par exemple, une monnaie libre imposée par un gouvernement n'est plus une monnaie libre.
  2.  **Liberté d'utiliser** : Si l'accès aux ressources est empêché, par exemple par un capitaliste ou par un gouvernement, alors monnaie libre ou non, cette liberté est supprimée.
  3.  **Liberté de produire** : Si les prix de vente sont imposés par des lois, alors on ne peut décider de la valeur de ce qu'on produit. Si les moyens de production sont monopolisés par un groupe restreint, alors on ne peut produire librement.
  4.  **Liberté d'échanger** : Si les échanges sont taxés, alors on ne peut échanger librement.

Un état imposant une monnaie libre et prélevant des impôts dessus rendrait donc cette monnaie non libre, en supprimant les libertés qu'elle permet.

Si l'on veut garantir ces libertés, des constructions supplémentaires à la monnaie libre sont utiles.
