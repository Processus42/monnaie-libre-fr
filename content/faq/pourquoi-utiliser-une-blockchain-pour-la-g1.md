---
title: Pourquoi utiliser une blockchain pour la Ğ1
description: "Parce que ça permet l'autonomie, et des économies d'énergie !"
---
Imprimer des billets ou frapper des pièces est une manière complexe et coûteuse de gérer la monnaie. À l’heure actuelle, on ne sait pas le faire d’une façon qui soit suffisamment fiable, sécurisée et résiliente. Le contrôle de l’émission de la monnaie sur un support matériel est toujours centralisé, et peut donc être corrompu ou violé.

C’est pourquoi la Ğ1 est une cryptomonnaie, c’est-à-dire une monnaie numérique décentralisée et sécurisée par cryptographie, grâce à la technologie <lexique>blockchain</lexique>.
De plus, Duniter (DU-uniter), le logiciel qui génère le DU, qui gère la monnaie et la toile de confiance, est un logiciel sous licence libre : son code est ouvert, accessible, et vérifiable par tous.

De plus l’algorithme duniter [consomme très peu d'énergie](faq/la-g1-est-elle-un-goufre-energetique)