---
title: Pourquoi ces noms Ğ1,  Duniter,  Césium ?
description: Origine...
---
## Ğ1

**Stéphane Laborde :** "Ğ a été choisie comme symbole du non-connu, de la nouveauté de la création de valeur économique indécidables, en référence au grand logicien Kurt Gödel pour [son théorème d'incomplétude](https://open.metu.edu.tr/bitstream/handle/11511/15409/index.pdf) établi en 1931, ainsi qu'au projet GNU lancé par Richard Stallman en 1983 (fondateur des logiciels libres qui sont restés et restent inconnus de beaucoup, alors qu'ils sont notamment l'armature de 99% d'Internet).
On peut consulter <https://www.glibre.org/> à ce sujet."

Le 1 parce que c'est la première monnaie libre.

## Duniter

C’est littéralement une machine à créer des <lexique>DU</lexique> comme un DUniteur

Il y a également une allusion à la dune du logo dont le sable se renouvelle avec le vent et qui semble être la même sans être vraiment la même…(c’est beau non ?)

## Cesium

Le DU est une part de monnaie créée à intervalles réguliers, donc lié au temps, temps mesuré par
l’atome de Cesium.

[Extrait de Wikipédia](https://fr.wikipedia.org/wiki/Temps_atomique_international) :

La seconde a été définie en 1967 lors de la 13e Conférence générale des poids et mesures comme étant la durée de 9 192 631 770 périodes de la radiation correspondant à la transition entre les deux niveaux hyperfins de l’état fondamental de l’atome de césium 133.