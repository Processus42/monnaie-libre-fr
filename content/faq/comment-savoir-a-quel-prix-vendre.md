---
title: Comment savoir à quel prix vendre ?
description: Différente méthodes pour définir votre prix de vente...
---
## Vous avez plusieurs possibilités pour vous aider à décider de votre prix de vente :

1. **Le prix du marché :** vous pouvez regarder sur ğchange **le prix médian** auquel se vend le type de bien ou service que vous proposez.
2. **Le prix relatif :** vous pouvez regarder sur ğchange le prix des biens ou services que vous souhaiteriez obtenir, et fixez le prix de ce que vous vendez relativement à l’estimation que vous faites de sa valeur, par rapport à la valeur de ce que vous souhaitez obtenir.\
   *Par exemple :* imaginons que vous souhaitiez acheter des BDs et vendre de la confiture. Si vous constatez qu’une BDs se vend en moyenne 10 DU, et que vous estimez que votre pot de confiture vaut bien la moitié d’une BD, alors vous pouvez essayer de vendre votre pot de confiture 5 DU. Rien ne garantit que vous réussirez à vendre vos confitures à ce prix là. Gardez en tête que **toute valeur est relative** : ce qui a beaucoup de valeur pour vous en a peut-être moins pour une personne, et plus pour une autre. Nous sommes probablement soumis au biais psychologique d’aversion à la dépossession, qui fait qu’on a tendance à surestimer la valeur de ce qu’on possède.
3. **La vente aux enchères :** Si vous pensez que votre bien est susceptible d’être rapidement convoité par de nombreuses personnes, vous pouvez organiser une vente aux enchères, afin d’obtenir le maximum de Ğ1 en échange de votre bien, ce qui vous évite de perdre trop de temps à tergiverser sur votre prix de vente, en laissant simplement “*le marché*” décider du prix qu’il est prêt à payer pour votre bien. Pour simplifier les choses, pensez aux [enchères de Vickrey.](https://fr.wikipedia.org/wiki/Ench%C3%A8re_de_Vickrey)
4. **Le prix libre :** Indiquez simplement dans votre annonce "*Prix Libre*" ainsi c'est l'acheteur qui déterminera le montant qu'il souhaite payer pour votre produit et/ou service.\
   De manière générale il a été constaté que les acheteurs sont bien plus généreux et proposent ainsi des tarifs supérieurs à ce que vous auriez imaginer demander.\
   *Il n'est pas rare de se voir proposer un montant correspondant à 3 fois ce que l'on n'osait pas demander !*