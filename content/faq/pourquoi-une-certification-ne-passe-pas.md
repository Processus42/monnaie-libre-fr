---
title: Pourquoi une certification ne passe pas
description: Voir les règles dans la licence
---
Quelques réponses à ceux qui se demandent pourquoi les certifications ne passent pas, pourquoi les certifications sont bloquées ou pourquoi l'adhésion d'un membre n'est pas validée.

## Délai de 5 jours

Il faut un délai de 5 jours entre deux certifications émises. Ce délai est compté à partir de la *date de prise en compte*, c'est-à-dire à partir du moment ou la certification n'est plus en "attente de traitement".  Cette date est bien souvent différente de celle à laquelle elle a été émise.

Vous pouvez voir la date à laquelle vos certifications émises sont prises en compte en regardant vos notifications dans césium. 
La prochaine certification n'est disponible que 5 jours après cette date. Si vous avez émis plusieurs certifications, il est impossible de déterminer laquelle passera en premier. 

Vous pouvez vérifier la disponibilité de chacun avec [wotwizard-UI](https://wotwizard.axiom-team.fr/fr/membres)

Notez que c'est à partir de la date d'émission de la certification qu'est comptée la durée de vie d'une certification : 2 ans, soit 730 jours et 12 heures. Il n'est donc pas conseillé d'émettre plein de certification d'un coup, elles ne seront pas prises en compte tout de suite, mais arriverons à échéance en même temps. 

Ce délai ne concerne que les certifications émises, vous pouvez en recevoir plusieurs en même temps, c'est même indispensable pour un nouveau. 

## Un nouveau doit avoir reçu 5 certifications

Et ces 5 certifications doivent **être disponibles en même temps**. Si une des 4 autres n'est pas disponible, aucune ne passera. 

Ces 5 certifications reçues doivent permettre de respecter la <lexique>règle de distance</lexique>. Si la règle de distance n'est pas respectée avec les **certifications disponibles**, ça ne passe pas. 

Il faut alors attendre que les certifications deviennent disponibles ou trouver d'autres certificateurs. 


## Un nouveau doit respecter la règle de distance.

Un ancien aussi, mais le problème se pose rarement pour les personnes déjà cocréatrices de monnaie.

G1-monit et wotwizard peuvent dire que la règle est respectée, mais pour cela, ils tiennent compte de toutes les certifications émises, y compris celles qui ne sont pas encore disponibles.  
Le nouvel entrant n'est pas validé tant que les certifications permettant de respecter la règle de distance ne sont pas disponibles. 

Vous pouvez vérifier que vos futures certifications vous permettent de respecter la règle de distance en utilisant le [calculateur](https://wot-wizard.duniter.org/23calculator).   
Attention ! 


## Un nouveau doit avoir fait sa demande d'adhésion.

Il est possible de certifier une personne qui a uniquement publié son identité. Sans faire de demande d'adhésion. 
C'est-à-dire qu'elle a juste rempli le champ *pseudonyme* dans Césium, ce champ est en fait l'identifiant public unique de la personne dans Duniter. 

Normalement la demande de transformation en compte membre dans Césium fait la publication d'identité + la demande d'adhésion. 
De même, quand on crée un *compte membre* via Césium, cela fait les deux. 


## Les nœuds peuvent être désynchronisés. 

Si vous voyez que la demande d'adhésion n'as pas été faite, ou si vous ne voyez pas les autres certifications de la personne, alors vous êtes peut-être sur un nœud désynchronisé, ou ces actions ont été faites sur un nœud désynchronisé.  

*Avant* toutes actions avec césium il est important de vérifier que les nœuds Duniter et Césium+ sur lesquels vous êtes sont bien synchronisés. Voir le [tuto sur le forum]( https://forum.monnaie-libre.fr/t/noeud-cesium-vs-noeud-duniter/14762)





