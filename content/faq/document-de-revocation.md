---
title: A quoi sert le document de révocation ?
description: "Indispensable en cas de perte ou vol des identifiants ou mot de
  passe du compte cocréateur. "
---
## Pas de procédure "mot de passe oublié" 
Dans la Ğ1 votre compte n'est pas géré par un organisme centralisateur, chacun est entièrement responsable de la gestion de son compte.  
Personne n'est en mesure de récupérer vos identifiant et mot de passe.  

<alert type="danger">Ce document est téléchargeable uniquement sur ordinateur, la version de césium pour smartphone est bugué. </alert>

## En cas de perte des identifiants ou mots de passe. 

Si vous ne pouvez plus accéder à votre compte, les G1 sont perdues, mais le compte continue à créer de la monnaie pour rien.    
Idem si vous vous faites pirater l'accès à votre compte cocréateur, c'est dommage de continuer à créer de la monnaie pour le pirate.   

Vous ne pouvez pas transformer un autre compte en cocréateur de monnaie tant que votre premier compte continue de créer de la monnaie.    
La seule possibilité en cas de perte ou vol d'identifiant est de révoquer le compte, en utilisant le *document de révocation.*
 
Il est important que ce document de révocation reste disponible même en cas de destruction de votre ordinateur.
Il faut le sauvegarder sur différent support, clé USB, cloud, etc. 
 
<alert type="warning">Ce document est  basé sur la date à laquelle vous avez demandé à devenir cocréateur (transformer un compte en compte membre).   
Donc si vous avez dû recommencer la procédure, vous devez télécharger à nouveau le doc de révocation (pensez à supprimer l'ancien devenu inutile).</alert>


## Utilisation 

Voir [Comment révoquer mon compte](/faq/comment-revoquer-mon-compte)
