---
title: Comment devenir cocréateur ?
description: "Procédure à suivre pour cocréer sa part de monnaie. "
---
## Pas d'urgence

Devenir cocréateur de monnaie n'est pas indispensable pour utiliser la monnaie libre.
Il suffit d'ouvrir un compte pour commencer à vendre et à acheter en Ğ1.
Il est tout aussi important de faire vivre la Ğ1 que de la créer.

## D'abord faire des rencontres

Pour devenir cocréateur, il faut être identifié en tant qu'humain vivant unique, par des cocréateurs qui vous connaissent.
Le meilleur moyen de connaître quelqu'un est de le rencontrer plusieurs fois.
Donc commencez par venir aux rencontres pour discuter et faire des échanges.
Vous trouverez sur la page d'accueil [l'agenda](/#agenda) des rencontres.
Quand les cocréateurs vous connaissent, vous pouvez leur demander de vous certifier.

## S'engager

Le futur cocréateur doit avoir pris le temps de lire et bien comprendre la [licence Ğ1](/licence-g1).
Il doit s'engager à respecter et à faire respecter cette licence.
Il s'engage entre autre à ne pas certifier ni re-certifier quelqu'un qui ne respecterait pas cette licence

## Cinq certifications

Pour devenir membre, il faut recevoir au moins 5 certifications et respecter la <lexique>règle de distance</lexique>.
Il est préférable d'avoir cinq cocréateurs prêts à vous certifier (5 promesses) avant de commencer la procédure.
Pour respecter la règle de distance, il est préférable d'avoir des connaissances venant de communautés différentes.

## Faire la demande de transformation en compte membre

Dans césium aller sur l'onglet "mon compte" puis bouton "options" choix "compte et sécurité" choix "transformer mon compte en compte membre".
Suivre la procédure, ne pas oublier de vérifier le téléchargement du [fichier de révocation](faq/comment-telecharger-le-fichier-de-revocation).
<iframe title="Césium : Comment passer d'un compte simple à un compte membre." src="https://video.tedomum.net/videos/embed/6b48e531-1af8-4d94-96cc-6011e9e14482" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="100%" height="405" frameborder="0"></iframe>

## Continuer à faire des rencontres

Devenir cocréateur n'est pas un but, c'est un début. Il est important de continuer à participer aux rencontres pour faire circuler la monnaie, pour rencontrer des nouveaux membres et continuer de recevoir des certifications.
