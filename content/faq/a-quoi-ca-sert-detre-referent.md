---
title: A quoi ça sert d'être référent ?
description: Les référents renforcent la toile de confiance.
---
## Cela n'apporte aucun avantage
Les cocréateurs <lexique title="référent">référents</lexique> n'ont aucun droit supplémentaire, aucun privilège, ils ne sont pas non plus des références en connaissance de la monnaie libre.   
On peut juste supposer qu'ils sont plus actifs dans la toile de confiance et qu'ils ont un peu d'expérience, car il ont reçu **et** émis un certain nombre de certifications.

## Pour la règle de distance
Il a été prouvé que, sur la planète, [tous les humains sont à moins de 5 ou 6 pas les uns des autres](https://fr.wikipedia.org/wiki/Six_degr%C3%A9s_de_s%C3%A9paration) (je connais quelqu'un qui connaît quelqu'un qui connaît quelqu'un ...).
Pour garder une certaine forme de sécurité dans la toile de confiance et surtout pour faciliter les calculs pour les serveurs Duniter, il a été jugé, pour la toile de confiance de la June, que si 100% des membres pouvaient rejoindre 80% des membres **référents** en moins de 5 pas, ce serait suffisant.
C'est ce qu'on appelle la <lexique>règle de distance</lexique> : une personne ne peut être membre que si elle arrive à rejoindre au moins 80% des membres référents en 5 pas ou moins.
Un membre référent étant un membre qui a émis **et** reçus un certain nombre de certifications en fonction de la taille de la toile de confiance.

Plus d'infos sur [cette présentation d'Eloïs](https://www.youtube.com/watch?v=8GaTKfa-ADU) lors des RML11 de Douarnenez en 2018.

Voir le lexique pour connaître le nombre de certifications nécessaires pour être <lexique>référent</lexique>.