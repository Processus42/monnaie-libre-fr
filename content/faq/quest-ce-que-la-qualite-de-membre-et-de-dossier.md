---
title: "Qu'est-ce que la qualité de membre et de dossier. "
description: "C'est une vérification de la règle de distance. "
---
## Notation

Suivant les outils ou logiciels utilisés la qualité est notée soit sous forme d'un nombre compris entre 0 et 1,25 soit sous la forme d'un pourcentage entre 0 et 100.   
- 80 % = 1 
- 100 % = 1,25  

Cette qualité est le résultat du calcul de la <lexique>règle de distance</lexique> dans la <lexique title="tdc">toile de confiance</lexique>


## Qualité de dossier

Pour un futur entrant la qualité de dossier correspond au nombre de membres <lexique title="référent">référents</lexique> dont la personne est à moins de **5** pas.  
Pour devenir membre, il faut une qualité de dossier >= 1 (80%).
Cette qualité est calculée à chaque renouvellement d'adhésion (chaque année).  

Une personne n'ayant pas cette qualité ne pourra pas devenir membre ou perdra son status de membre lors du renouvellement de son adhésion. Cela peut arriver si un de ses certificateurs à perdu des certifications importantes.  
Il lui faudra trouver d'autres certificateurs. 

Cette qualité s'appelle *distance* dans [wotwizard] (https://wot-wizard.duniter.org/20distances)   
On peut vérifier la qualité d'un nouveau dossier dans [g1-monnit](https://monit.g1.nordstrom.duniter.org/willMembers?lg=fr)
Ces informations sont désormais regroupées sur le site [wotwizard-ui](https://wotwizard.axiom-team.fr/)


## Qualité de membre

Cette qualité correspond au nombre de référents dont la personne est à moins de **4** pas.   
Une personne certifiée par un membre ayant une qualité supérieure ou égale à 1 (80%) aura automatiquement une qualité de dossier suffisante.  
Avoir un certificateur de qualité >= 1 est suffisant pour avoir une qualité de dossier respectant la règle de distance, mais n'est pas indispensable. On peut avoir une qualité de dossier >=1 en ayant des certificateurs de qualités moindres venant d'horizons différents, ne venant pas tous du même groupe local. 

## Calculateur
Il existe maintenant un outil permettant de calculer cette qualité avant de commencer les certifications : [calculateur](https://wot-wizard.duniter.org/23calculator)  
Il suffit d'entrer tous les pseudos des éventuels futurs certificateurs dans la zone "Nouveaux certificateurs :" et de cliquer sur "ok". Après quelques longues secondes, vous aurez le résultat.   
 Si c'est inférieur à 80% ce n'est pas la peine de commencer les certifications, trouvez d'abord d'autres certificateurs vous permettant d'atteindre au moins 80%.