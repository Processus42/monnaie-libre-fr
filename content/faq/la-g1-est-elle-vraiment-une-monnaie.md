---
title: La Ğ1 est-elle vraiment une monnaie ?
description: Oui,  mais pas pour la loi.
---


Ça dépend de la définition ! 
Si on définit une monnaie comme un  facilitateur d'échanges, une unité de mesure de valeur, alors **oui**.

La loi française ne reconnaît pas la Ğ1 comme une monnaie (de même pour le Bitcoin et les monnaies locales). Cependant, la loi est normative et non descriptive, et n'est ni suffisant ni pertinent pour décrire le monde.

La Ğ1 n'est pas matérialisée en billets. Cela ne veut pas dire que ce n'est pas une monnaie : la monnaie n'est pas forcement matériel, la plupart des euros ne sont que des chiffres sur des comptes. De plus, les billets de banque ne sont pas à proprement parler de la monnaie : ils ne sont que des reconnaissances de dette que la banque émet, les "vrais" euros étant des valeurs informatiques dans les ordinateurs des banques !

