---
title: Comment la Ğ1 crée du lien social
description: Il faut rencontrer les autres membres pour participer
---
### Lien social
Pour garantir à toute la communauté que chaque personne ne va créer qu’une seule fois sa part de monnaie, il faut un système d'identifications. C'est pour cela qu'une toile de confiance a été créée. C'est un système d'identification décentralisé.  
 
Chaque nouveau membre de la communauté doit être connu et reconnu par 5 personnes qui sont déjà membres, qui se portent garantes de l’unicité du compte sur lequel est généré le DU quotidien du nouveau membre.  
Cette reconnaissance ne peut se faire qu’entre êtres humains réels, et non par des machines ou une entité centrale chargée d’émettre des documents d’identification, de ce fait les gens sont amenés à se connaître et à se rencontrer, renforçant ainsi le lien social et les opportunités de faire des échanges.
