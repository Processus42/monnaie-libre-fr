---
title: " La Ğ1 est-elle sécurisée ?"
description: Oui grace à la blockchain
---
La Ğ1 est basée sur la technologie de la <lexique>blockchain</lexique>. **Une blockchain est infalsibiable**

Une blockchain, ou “chaîne de blocs” en français, est un grand livre de compte signé par cryptographie.   

Les créateurs de la Ğ1 ont choisi cette technologie car elle présente de nombreux avantages :

* Un très faible coût énergétique.
* Les transactions sont infalsifiables.
* Il n’y a pas d’entité centrale qui contrôle le réseau ou la monnaie.
* Pour peu que l’utilisateur choisisse des phrases secretes suffisamment longues et compliquées, on ne peut pas lui voler sa monnaie.

