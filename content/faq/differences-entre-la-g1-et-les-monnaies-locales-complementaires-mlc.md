---
title: " Différences entre la Ğ1 et les monnaies locales complémentaires (MLC)"
description: Les MLC sont des euros déguisés
---
Les monnaies locales complémentaires telles que celles autorisées par [la loi du 31 juillet 2014](https://www.legifrance.gouv.fr/jorf/article_jo/JORFARTI000029313554), sont adossées à l'Euro.

A chaque unité monétaire d'une MLC est associé un Euro sur un compte en banque. Ces MLC ne sont finalement que des euro habillés d'une touche locale. 

Pour acquérir des unités de monnaies locales il faut le plus souvent les acheter en payant en Euros.   
Pour acquérir des Ğ1 il faut vendre des biens ou services, mais chacun peut devenir cocréateur de monnaie. 


Alors que 1 Sol-Violette = 1 Euro, le cours Ğ1/Euro n’est pas défini légalement.



Lorsque des utilisateurs troquent des Euros contre des Ğ1, ils appliquent librement le taux  de change qu'ils veulent à l'instant de l'échange.