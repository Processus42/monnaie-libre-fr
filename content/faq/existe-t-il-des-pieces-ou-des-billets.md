---
title: "Existe-t-il des pièces ou des billets ? "
description: Seulment quelques experimentations.
---
## Rien d'officiel

De toute façon, la monnaie libre étant totalement décentralisée, il n'y aura jamais rien d'officiel.  
À ce jour, il n'y a rien non plus qui fasse consensus entre la plupart des junistes. 
Il n'existe que quelques initiatives locales où ponctuelles.


## Les billets ne sont qu'une promesse de monnaie
Il faut toujours garder en tête que les pièces et les billets ne seront jamais des vraies ğ1, les vraies ğ1 sont uniquement celles inscrites dans la blockchain.  
Il y a toujours un risque de falsification de ces billets

### Les billets sans code d'accès. 
Ces billets sont émis par des associations locales, pour recevoir les vraies ğ1 il faut rapporter les billets auprès de l'association émettrice. 
Vous n'avez aucun moyen de vérifier que l'association n'a pas émis plus de billets qu'elle n'a de ğ1 en réserve. Aucune garantie non plus que le compte de l'association ne soit jamais piraté, ni que les ğ1 ne soient pas dérobées. 

### Les billets avec codes d'accès
Ces billets peuvent être émis par des utilisateurs ou des associations. Chacun de ces billets correspond à un portefeuille unique. Le code d'accès au portefeuille est caché, par un truc à gratter ou un collage à déchirer, et permet de récupérer directement les ğ1, sans avoir à retrouver l'émetteur du billet.   
 
Normalement, un QR-code ou une clé publique permet de vérifier que les ğ1 sont toujours sur le compte, ce qui est une petite garantie supplémentaire. 


## Quelques unes des initiatives locales
- les coupons de vies
- les bons d'échanges de Lodève (BEL)
- La Galine
- les Ğ1billets 

Ces initiatives sont très peu développées, et peut-être déjà abandonnée pour certaines.

## Les pièces
À ce jour, il n'existe aucune pièce de monnaie ğ1.   
Le cout de fabrication serait trop élevé. 
