---
title: Contribuer
description: Toutes les informations pour participer !
plan: ui/main-flatplan
---
## Ils changent le monde

On entend parfois certains <lexique>junistes</lexique> dire que la Ğ1 est "*une œuvre d'art collective*".

Et c’est vrai qu’il y a beaucoup de gens qui font des choses autour des monnaies libres en général ou de la Ğ1 en particulier.

C’est toute une symphonie qui se met en place, sans que personne sache trop comment.

Si la Ğ1 existe aujourd’hui, c’est grâce à tous ces gens :

Ceux qui développent les logiciels : vous trouverez leurs noms sur le site de [Duniter](https://duniter.org/fr/), le moteur de la blockchain.

Ceux qui valident les transactions de la blockchain en [forgeant des blocs](https://duniter.org/fr/miner-des-blocs/).

Ceux qui traduisent les logiciels.

Ceux qui rédigent de la documentation.

Ceux qui rédigent des rapports de bug.

Ceux qui développent l’économie : ce sont tous ceux qui, [par leurs échanges](https://www.gchange.fr/#/app/market/lg?last), valorisent la Ğ1, et prouvent au quotidien qu’une monnaie libre permet d’échanger.

Ceux qui en parlent autour d’eux.

Ceux qui créent des groupes locaux.

Ceux qui organisent des parties de [Ğeconomicus](http://geconomicus.glibre.org/).

Ceux qui créent des mèmes rigolos.

Ceux qui donnent des conférences pour expliquer la création monétaire.

Ceux qui créent du contenu pédagogique en ligne.

Ceux qui écrivent des livres.

Ceux qui font de la création graphique.

Ceux qui mettent à disposition des infrastructures permettant d’héberger des contenus.

Et tous ceux que cette liste a oublié de lister.

## À vous de jouer !

La Ğ1 n’appartient à personne.

D’ailleurs, si on vous a dit que ce site était le “*site officiel*” des monnaies libres ou de la Ğ1, alors mettons ça au clair toute de suite : ni la monnaie libre ni la Ğ1 ne peuvent avoir de “*site officiel*“, car il n’y a pas d’autorité qui les gouverne.

Certes, ce site est souvent celui qui sort en premier dans les moteurs de recherche lorsque vous tapez “*monnaie libre*“, mais d’autres sites parlent des monnaies libres ou de la Ğ1 et ils ne sont pas moins légitimes que celui-ci pour le faire.

Comme pour “gilet jaune”, certaines personnes peuvent essayer de déposer la marque “monnaie libre”, mais ils se feront probablement retoquer, car “monnaie libre” n’est que l’association de deux noms communs.

La Ğ1 n’a pas de tête.

La Ğ1 n’a pas de centre.

Vous êtes donc libres de contribuer comme bon vous semble, sans attendre que qui que ce soit vous en donne l’autorisation.

Si vous souhaitez collaborer avec d’autres gens sur certains projets, vous pouvez vous manifester sur n’importe quelle plateforme ou réseau sur lequel se trouvent des junistes : les junistes sauront en général assez bien vous mettre en relation avec d’autres junistes pour que vous puissiez joindre vos efforts 😉

## Faire des dons.

### Des dons en DUğ1.
Vous pouvez donner des DUğ1 aux contributeurs techniques et aux <lexique title="forgeron">forgeurs</lexique> en copiant collant les clés suivantes :  
Contributeurs tech. Duniter/Ğ1 : 78ZwwgpgdH5uLZLbThUQH7LKwPgjMunYfLiCfUCySkM8      
Remuniter, pour les forgerons : TENGx7WtzFsTXwnbrPEvb6odX2WnqYcnnrjiiLvp1mS

Il existe tout un tas de cagnottes de cotisation pour divers projets, vous en trouverez une [liste non exhaustive sur le forum](https://forum.monnaie-libre.fr/t/liste-des-cagnottes-de-cotisations-de-la-g1/13828).  
Attention, avant de faire un don, assurez-vous de qui gère vraiment la cagnotte.

### Donner d'autres monnaies
Il existe aussi des associations qui aident au développement de la Ğ1, et acceptent les dons et cotisations dans d'autres monnaies :    
[axiom-team](https://axiom-team.fr)    
[éconolibre](https://www.helloasso.com/associations/econolibre/formulaires/1/widget)  
Toute aide même minime est la bienvenue.


## Soutenir financièrement la Ğ1v2
La Ğ1 évolue, pour avoir des logiciels plus fluides, plus pratiques, plus résilients, aidez financièrement en euros les développeurs de la Ğ1 du futur.   
Le [Financement participatif](https://duniter.fr/blog/financement-participatif/) est aujourd'hui terminé, mais les développeurs ont toujours besoin d'euros, vous pouvez les aider via [Hello asso](https://www.helloasso.com/associations/axiom-team) Même une toute petite somme montrera votre soutien. 


