---
title: Découvrir la monnaie libre
description: Toutes les informations pour comprendre !
plan: ui/main-flatplan
---
## La monnaie n'est pas le fruit du travail.

La monnaie que vous avez n'est pas le fruit de votre travail.
La monnaie, c'est ce que vous acceptez en **échange** du fruit de votre travail.

## Mais alors qui produit la monnaie ?

Dans le système monétaire régi par les banques, ce sont les banques commerciales qui créent cette monnaie que vous utilisez tous les jours.
Le gouvernement vous le dit lui-même <https://www.economie.gouv.fr/facileco/creation-monetaire-definition> \
C'est la banque qui décide qui a le droit à la monnaie, selon ses critères (généralement un max de profit).
Bien sûr, cette création ne se fait pas gratuitement, il y a des intérêts à payer.
Globalement, il faut rembourser plus qu'il n'y a de monnaie en circulation.

Conséquence : Course au profit, compétition, faillite pour les plus fragiles. Cette course au profit entraîne entre autres l'épuisement des ressources et de la biodiversité.

## Et la monnaie libre ?

Dans le système de la monnaie libre, la monnaie est créée à parts égales pour tous les membres, sans dette et sans intérêts à rembourser.
Chaque membre créant sa propre portion de monnaie, c'est en quelque sorte un revenu de base, ce n'est pas un revenu de suffisance. La suffisance étant une notion subjective, propre à chaque individu.
La création permanente de monnaie, allège la peur de l'avenir, et favorise un comportement de collaboration et d'entraide.

## En vidéo

La monnaie libre sans jargon par Corinne :

<iframe src="https://player.vimeo.com/video/498611617" width="100%" height="420" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>

17 membres expliquent la Monnaie Libre :

<iframe width="100%" height="410" sandbox="allow-same-origin allow-scripts allow-popups" src="https://tube.p2p.legal/videos/embed/ea3c06b6-134b-49da-aea2-7a8bacf97dd5" frameborder="0" allowfullscreen></iframe>

- - -

## Une unité de mesure

Nous utilisons la monnaie comme unité de mesure. Mais comment appliquer cette unité de mesure si elle ne représente pas la même valeur dans l'espace et dans le temps.

Le mètre a été défini comme la distance de l'équateur au pôle divisé par 10 000 000. Le degré Celsius comme la différence de température entre le gel et l'évaporation de l'eau divisé par 100.\
La seconde du SI est définie par la durée d'un certain nombre d'oscillations (9 192 631 770 exactement) liées à un phénomène physique concernant l'atome de césium.\
Ces unités de mesure sont invariantes et universelles pour tous les êtres humains dans le temps et l'espace.

Pourtant, nous n'avons pas de repères pour définir la valeur d'une unité monétaire. Cette valeur varie dans le temps et l'espace. Une monnaie peut être dévaluée par simple création d'unités supplémentaires.\
Avec la monnaie libre, nous fabriquons **une nouvelle unité de mesure** : *la quantité de monnaie produite chaque jour par chaque individu*. Quand la monnaie libre sera pleinement en place, cette quantité représentera toujours la même *portion* de monnaie par rapport à la masse monétaire globale. Ce qui en fait un invariant à travers le temps et l'espace.\
**Le DUğ1 devient une unité de mesure de valeur universelle.**
