---
title: La monnaie libre et les crypto-monnaies
description: La Ğ1 est une crypto-monnaie, mais pas comme les autres !
---
## Cryptomonnaie.

Les cryptomonnaies sont les monnaies basées sur la technologie de la blockchain décentralisée.\
La plupart des cryptomonnaies connues sont conçues pour être très spéculatives. Leur création est souvent réservée aux personnes qui en ont les moyens techniques ou financiers.

Ce qui différencie la Ğ1 c'est son mode de création. La Ğ1 est créée à part égale entre tous les membres de la communauté.

## Économique et écologique.

Imprimer des billets ou frapper des pièces est une manière complexe et coûteuse de gérer la monnaie. À l’heure actuelle, on ne sait pas le faire d’une façon qui soit suffisamment fiable, sécurisée et résiliente. Le contrôle de l’émission de la monnaie sur un support matériel est toujours centralisé, et peut donc être corrompu ou violé.

C’est pourquoi la Ğ1 est une cryptomonnaie, c’est-à-dire une monnaie numérique décentralisée et sécurisée par cryptographie, grâce à la technologie blockchain.\
De plus, Duniter (DU-uniter), le logiciel qui génère le DU, qui gère la monnaie et la toile de confiance, est un logiciel sous licence libre : son code est ouvert, accessible et vérifiable par tous.