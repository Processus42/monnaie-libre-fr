---
title: Comprendre
description: Les explications théoriques
plan: ui/main-flatplan
---
## La TRM

La monnaie libre est l'application de la Théorie Relative de la Monnaie écrite en 2010 par Stéphane Laborde.

### 4 libertés

Cette théorie pose 4 axiomes qui sont les 4 libertés économiques

1. La liberté de choix de son système monétaire
2. La liberté d’utiliser les ressources
3. La liberté d’estimation et de production de toute valeur économique
4. La liberté d’échanger, comptabiliser, afficher ses prix “dans la monnaie”

La liberté s'entend bien sûr au sens de non-nuisance à soi et à autrui. S'il y a nuisance, il y a privation de liberté.

### Renouvellement des générations

![Les humains du passé n'existent plus, les humains du futur n'existe pas encore](https://www.creationmonetaire.info/wp-content/uploads/2013/05/TRM35_6.png "Espace-Temps humain (espace de vie(t) des âges de 0 à ev en vert)")
Les vivants qui arrivent en fin de vie (ev≈80ans), ont pu échanger avec des humains du passé (en rouge), mais n'échangeront jamais avec les humains du futur.\
Les humains en début de vie n'ont jamais échangé avec les humains du passé, mais échangeront un jour avec les humains du futur (en jaune).\
Seul les humains vivants (en vert) peuvent faire des échanges entre eux, il n'y a pas de raison que leurs échanges influent sur les générations futures, ni qu'ils soient impactés par les échanges des générations passées.

### Égalité spatio-temporelle

Pour respecter les 4 libertés, il faut que chaque humain crée la même part de monnaie (la même portion).\
Chaque génération crée la monnaie qu'elle utilise sans que cette monnaie ait un impact sur les générations futures.

### La formule

DU=c(M/N)\
La part que chaque être humain crée (DU) est une portion, un coefficient (c) de la moyenne de la masse monétaire par membre (M/N).\
Ce coefficient **c** doit être proche de 10% par an pour ne pas privilégier les plus jeunes ni les plus âgés.

### Convergence des comptes

Chaque créateur créant la même quantité de monnaie, leurs comptes se rapprochent relativement. Comme une différence d'âge devient de moins en moins visible en vieillissant.

![](/uploads/convergence-des-soldes.png)

## Plus d'informations



#### La TRM.

Pour en savoir plus vous pouvez lire la Théorie Relative de la Monnaie. Ce livre écrit par Stéphane Laborde est disponible en ligne : [http://trm.creationmonetaire.info/](http://trm.creationmonetaire.info/)   

#### La TRM en détail.

Emmanuel Bultot, docteur en mathématiques, a publié [« la TRM en détail »](http://monnaie.ploc.be/#trm-en-detail), une revisite de la Théorie Relative de la Monnaie depuis un nouveau point de vue, très bien réalisée et vivement recommandée.

#### La TRM pour les enfants.
David Chazalviel, ingénieur informatique, a réalisé [« la TRM pour les enfants »](http://cuckooland.free.fr/LaTrmPourLesEnfants.html) une explication de la TRM interactive plus facile à comprendre. 

### En vidéo 

Une vidéo explicative de la TRM en 1 h 15.

<iframe width="560" height="315" src="https://www.youtube.com/embed/PdSEpQ8ZtY4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## La Ğ1

La Ğ1 (la "June") est la première MONNAIE LIBRE de l'histoire de l'humanité.\
Conformément à la Théorie Relative de la Monnaie (TRM), elle est cocréée sans dette, et à parts égales, entre tous les êtres humains de toutes les générations présentes et à venir, sous la forme d'un "paquet" de monnaie, une quantité de Ğ1 (une quantité de "junes"), appelée DIVIDENDE UNIVERSEL (DU).\
Les enfants sont des êtres humains à part entière, ils participent à la création monétaire en créant tous les jours leur part de monnaie !

### Le DUğ1

Pour la monnaie libre G1 le DU est calculé tous les semestres (182.625 jours) le coefficient est donc de 4,88% par semestre, et il est réparti sur tous les jours.\
Tous les jours, chaque cocréateur crée une petite portion de Dividende Universel.\
La masse monétaire de départ étant à zéro, le premier DU a été fixé arbitrairement à 10 Ğ1 par jour, et depuis il augmente.

### La formule adaptée

La formule de calcul du DUğ1 est : **DU<sub>t+1</sub> = DU<sub>t</sub> + (c<sup>2</sup> × (M/N)<sub>t</sub> / 182.625)**\
Ce calcul est fait tous les six mois (182,625 jours) il tient compte de l'évolution du nombre de membres pendant la phase d'installation de la monnaie.

### La blockchain
La technologie blockchain a été choisie pour sa simplicité de mise en place, sa sécurisation et sa décentralisation.\
Contrairement aux idées reçues ce n'est pas la blockchain qui consomme de l'énergie, mais la preuve de travail. \
Pour la Ğ1 l'algorithme a été [**conçu pour consommer vraiment très peu d'énergie**](https://duniter.fr/faq/duniter/duniter-est-il-energivore/), un ordinateur de la taille d'un paquet de cigarette suffit pour calculer les blocs, bien loin des immenses fermes à bitcoin.

### La toile de confiance
La monnaie étant créée sur les comptes des utilisateurs membres, il faut s'assurer que chacun ne possède qu'un seul compte créateur de monnaie.
Pour respecter la décentralisation et ne donner le pouvoir à aucun organisme, ce sont les membres eux-mêmes qui identifient les autres membres
