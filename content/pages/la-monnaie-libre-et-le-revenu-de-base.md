---
title: La monnaie libre et le revenu de base
description: La monnaie libre est un revenu de base, pas un revenu d'existance
---
TODO

* Expliquer le concept de revenu de base. La ML est compatible avec la définition donnée par le MFRB
* Différence entre revenu de base, revenu d'existence

Pas besoin de se torturer l'esprit pour se demander où, et auprès de qui, collecter de la monnaie à redistribuer pour financer un revenu de base. Le DU de la monnaie libre EST un revenu de base, créé directement sur le compte de chacun-e. C'est tellement plus simple !