---
title: La monnaie libre et les monnaies locales
description: Différence entre la monnaie libre et les monnaies locales
---
## La monnaie locale complémentaire n’est pas alternative

Elle n’est pas créée autrement que la monnaie dette : il faut payer un euro pour en obtenir une unité monétaire et inversement. Si l’euro s’effondre ou subit une inflation, les MLC aussi.

La monnaie locale n’est pas une garantie de la localisation de l’économie. L’Euro (monnaie locale de la zone Euro) n’empêche en rien l’achat de produits venant d’Amérique ou d’Asie. \
Il est très facile d’utiliser la monnaie locale dans certaines supérettes pour acheter des bananes ou du chocolat venant de l’autre bout du monde. Ces supérettes se servent de la monnaie "locale" pour attirer des clients et convertissent une grande partie cette monnaie en Euro.
Finalement, la monnaie locale s’avère être des bons d’achats pour un groupement de magasins.

Si vous voulez vraiment développer l’économie locale, il est plus judicieux de regarder la provenance des produits que vous achetez. Préférez acheter des pommes en euros au producteur du coin que des bananes en monnaie locale à la supérette (même bio).

## La monnaie libre n’est pas forcément locale

Ensuite, une monnaie libre n’est pas nécessairement locale. Par exemple, la Ğ1, grâce à son infrastructure informatique, est potentiellement mondiale. Le seul frein à cette expansion est la toile de confiance (dont les règles peuvent évoluer).

## Une monnaie libre peut être locale.

Il est toutefois possible de créer une monnaie locale complémentaire en se basant sur la monnaie libre au lieu de l’Euro. On peut créer une monnaie locale adossée à la Ğ1 ou toute autre monnaie libre (quand il y en aura d’autres).\
Une telle monnaie n’aurait de locale que le nom, car convertible facilement en monnaie internationale.

On peut aussi concevoir une monnaie locale indépendante basée sur la <lexique>théorie relative de la monnaie</lexique>. Cette monnaie-là serait vraiment locale, car non convertible facilement et à usage uniquement local.
