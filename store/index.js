export const state = () => ({
  rootPath: null,
  footer: null,
})

export const mutations = {
  SET_ROOTPATH(state, payload) {
    state.rootPath = payload
  },
  SET_FOOTER(state, payload) {
    state.footer = payload
  },
}

export const actions = {
  async nuxtServerInit({ commit }, { isDev, $content }) {
    if (isDev) commit('SET_ROOTPATH', process.env.PWD)

    const footer = await $content('ui/footer').fetch()
    commit('SET_FOOTER', footer)
  },
}
